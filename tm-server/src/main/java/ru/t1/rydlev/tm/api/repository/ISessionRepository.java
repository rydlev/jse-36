package ru.t1.rydlev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.model.Session;

import java.sql.ResultSet;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

    @NotNull
    Session fetch(@NotNull ResultSet row) throws Exception;

    @NotNull
    Session add(@NotNull Session session) throws Exception;

    @NotNull
    Session add(@NotNull String userId, @NotNull Session session) throws Exception;

    void update(@NotNull Session session) throws Exception;

}
