package ru.t1.rydlev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.api.repository.IRepository;
import ru.t1.rydlev.tm.enumerated.Sort;
import ru.t1.rydlev.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    @NotNull
    List<M> findAll(@Nullable Sort sort) throws Exception;

    @Nullable
    M removeById(@Nullable String id) throws Exception;

    @Nullable
    M removeByIndex(@Nullable Integer index) throws Exception;

}
