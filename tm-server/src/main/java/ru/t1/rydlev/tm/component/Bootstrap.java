package ru.t1.rydlev.tm.component;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import lombok.Getter;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.lifecycle.SingletonResourceProvider;
import org.apache.cxf.jaxws.JaxWsServerFactoryBean;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.t1.rydlev.tm.api.component.IBootstrap;
import ru.t1.rydlev.tm.api.endpoint.*;
import ru.t1.rydlev.tm.api.repository.IProjectRepository;
import ru.t1.rydlev.tm.api.repository.ISessionRepository;
import ru.t1.rydlev.tm.api.repository.ITaskRepository;
import ru.t1.rydlev.tm.api.repository.IUserRepository;
import ru.t1.rydlev.tm.api.service.*;
import ru.t1.rydlev.tm.endpoint.*;
import ru.t1.rydlev.tm.enumerated.Role;
import ru.t1.rydlev.tm.model.User;
import ru.t1.rydlev.tm.repository.ProjectRepository;
import ru.t1.rydlev.tm.repository.SessionRepository;
import ru.t1.rydlev.tm.repository.TaskRepository;
import ru.t1.rydlev.tm.repository.UserRepository;
import ru.t1.rydlev.tm.service.*;
import ru.t1.rydlev.tm.util.SystemUtil;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class Bootstrap implements IBootstrap, IServiceLocator {

    @NotNull
    private final static Logger LOGGER_LIFECYCLE = LoggerFactory.getLogger("LIFECYCLE");

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final Connection connection = connectionService.getConnection();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository(connection);

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository(connection);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService);

    @NotNull
    private final IUserService userService = new UserService(propertyService, connectionService, projectService, taskService);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);

    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final List<IEndpoint> endpoints = Arrays.asList(
            authEndpoint, systemEndpoint, projectEndpoint, taskEndpoint, userEndpoint
    );

    @NotNull
    private final JacksonJaxbJsonProvider jsonProvider = new JacksonJaxbJsonProvider();

    {
        soap(endpoints);
        rest(endpoints);
    }

    private void rest(@NotNull final Collection<IEndpoint> endpoints) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String baseUrl = "http://" + host + ":" + port + "/";
        final JAXRSServerFactoryBean bean = new JAXRSServerFactoryBean();
        bean.setProviders(Collections.singletonList(jsonProvider));

        bean.setResourceClasses(endpoints
                .stream()
                .map(IEndpoint::getClass)
                .collect(Collectors.toList())
        );

        endpoints.forEach(endpoint -> {
            final SingletonResourceProvider provider = new SingletonResourceProvider(endpoint);
            bean.setResourceProvider(endpoint.getClass(), provider);
        });

        bean.setAddress(baseUrl);
        bean.create();
    }

    public void soap(@NotNull final Collection<IEndpoint> endpoints) {
        endpoints.forEach(this::soap);
    }

    private void soap(@NotNull final IEndpoint endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        final JaxWsServerFactoryBean svrFactory = new JaxWsServerFactoryBean();
        svrFactory.setAddress(url);
        svrFactory.setServiceBean(endpoint);
        svrFactory.create();
        System.out.println(url);
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    public static HazelcastInstance instance() {
        @NotNull final Config config = new Config();
        @NotNull final NetworkConfig network = config.getNetworkConfig();
        network.setPort(5701).setPortCount(20);
        network.setPortAutoIncrement(true);
        @NotNull final JoinConfig join = network.getJoin();
        join.getMulticastConfig().setEnabled(false);
        join.getTcpIpConfig().addMember("localhost").setEnabled(true);

        return Hazelcast.newHazelcastInstance(config);
    }

    private void initPID() {
        try {
            @NotNull final String filename = "task-manager.pid";
            @NotNull final String pid = Long.toString(SystemUtil.getPID());
            Files.write(Paths.get(filename), pid.getBytes());
            @NotNull final File file = new File(filename);
            file.deleteOnExit();
        } catch (@NotNull IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {
        initPID();
        LOGGER_LIFECYCLE.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        LOGGER_LIFECYCLE.info("*** TASK MANAGER IS SHUTTING DOWN ***");
    }

}
