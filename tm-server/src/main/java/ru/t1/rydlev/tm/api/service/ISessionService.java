package ru.t1.rydlev.tm.api.service;

import ru.t1.rydlev.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
