package ru.t1.rydlev.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rydlev.tm.api.service.IServiceLocator;
import ru.t1.rydlev.tm.dto.request.AbstractUserRequest;
import ru.t1.rydlev.tm.enumerated.Role;
import ru.t1.rydlev.tm.exception.EndpointException;
import ru.t1.rydlev.tm.exception.system.AccessDeniedException;
import ru.t1.rydlev.tm.model.Session;

@Getter
@NoArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    protected IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected Session check(
            @Nullable final AbstractUserRequest request
    ) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        try {
            return serviceLocator.getAuthService().validateToken(token);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    protected Session check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @Nullable Session session;
        try {
            session = serviceLocator.getAuthService().validateToken(token);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

}
