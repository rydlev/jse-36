package ru.t1.rydlev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.dto.Result;
import ru.t1.rydlev.tm.dto.response.*;
import ru.t1.rydlev.tm.dto.request.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@WebService
@Path("/api/UserEndpoint")
public interface IUserEndpoint extends IEndpoint {

    @POST
    @NotNull
    @WebMethod
    @Path("/ping")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Result ping();

    @POST
    @NotNull
    @WebMethod
    @Path("/changeUserPassword")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserChangePasswordResponse changeUserPassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserChangePasswordRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/lockUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLockRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/registryUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserRegistryResponse registryUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRegistryRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/removeUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserRemoveRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/unlockUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUnlockRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/updateUserProfile")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserUpdateProfileResponse updateUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserUpdateProfileRequest request
    );

}
